//We are using baudio to make sounds
var baudio = require('baudio');

var downward = function (t, duration, opts) {
  opts = opts || {}
  if (!opts.frequency) {
    opts.frequency = 440
  }
  return Math.sin(t*opts.frequency + 440*Math.sin(2*t));
}

var upward = function (t, duration, opts) {
  opts = opts || {}
  if (!opts.frequency) {
    opts.frequency = 440
  }
  return Math.sin(t*opts.frequency - 440*Math.sin(2*t));
}

var FMThing = function (t, duration, opts) {
  opts = opts || {}
  var A = 0.75;
  //var F = 2000;
  var F = opts.frequency || 2000
  var B = 5;
  var F2 = 1;

  H1 = A*Math.sin(F*t+B*(Math.sin(F2*t-Math.PI/2)+1));
  H2 = 0.5*A*Math.sin(2*F*t+B*(Math.sin(F2*t-Math.PI/2)+1));
  H3 = 0.25*A*Math.sin(3*F*t+B*(Math.sin(F2*t-Math.PI/2)+1));
  H4 = 0.125*A*Math.sin(4*F*t+B*(Math.sin(F2*t-Math.PI/2)+1));

  return H1+H2+H3+H4;
}

var additative = function (t, duration, opts) {
  opts = opts || {}
  if (!opts.frequency) {
    opts.frequency = 220
  }
    return Math.sin(opts.frequency*t) + 1/3*Math.sin(3*opts.frequency*t) + 1/5*Math.sin(5*opts.frequency*t) + 1/7*Math.sin(7*opts.frequency*t) + 1/9*Math.sin(9*opts.frequency*t) + 1/11*Math.sin(11*opts.frequency*t);// + 1/13*Math.sin(13*opts.frequency*t);
}

//This gives an envelope function to prevent clicks when the notes change
//Currently I am just using a Hamming window
var envelope = function(t, length) {
  var alpha = 0.54;
  var beta = 0.46;
  var lower = 0.05*length;
  var upper = 0.95*length;
  if ((t % 1) < lower) {
    return alpha-beta*Math.cos(2*3.141592*(t%length));
  } else if (lower < (t % length) && (t % length) < upper) {
    return 1;
  } else {
    return alpha-beta*Math.cos(2*3.141592*(t%length));
  }
}

/*
  This is going ot be a configurable envelope that follows the
  attack-decay-sustain-release pattern for musical instruments.
  we can change the shapes used for each stage. First try linear.

  The note starts at 0 (no sound), the sound grows to some maximum amplitude
  before decaying slightly to a height for the sustain phase. This is
  maintained for the majority of the note. At the end the note decays to zero
  amplitude.

  The lengths and shapes of the different phases and the height of the sustain
  phase play a large part of how a synthesised instrument sounds.

  Parameters:
  attack time - proportion of total time used by the attack phase
  attack time max - maximum duration of the attack phase
  decay time - proportion of the total time used for the decay period
  decay time max - maximum duration of the decay phase
  sustain height - the height of the sustain section, taken from [0,1]
  release time - proportion of the total time used by the release phase
  release time max - maximum duration for the release time
*/
var ADSRenvelope = function (t, length, opts) {
  opts = opts || {}
  opts.ATime = opts.ATime || 0.1
  opts.ATimeMax = opts.ATimeMax || 1
  opts.DTime = opts.DTime || 0.1
  opts.DTimeMax = opts.DTimeMax || 1
  opts.sustainHeight = opts.sustainHeight || 0.8
  opts.RTime = opts.RTime || 0.3
  opts.RTimeMax = opts.RTimeMax || 1

  if (t - noteStart < opts.ATime*length && t - noteStart < opts.ATimeMax) {
    // Attack phase
    // For a linear envelope return t*slope
    // Find slope. Rise over run. The final height is always 1 for the attack
    // phase.
    var slope = opts.ATime*length < opts.ATimeMax ? 1/opts.ATime*length : 1/opts.ATimeMax
    // Slope is 1/(opts.ATime*duration)
    return (t - noteStart)*slope
  } else if (t - noteStart < (opts.ATime+opts.DTime)*length && t - noteStart < opts.ATimeMax+opts.DTimeMax) {
    // Decay phase
    var attackDuration = opts.ATime*length < opts.ATimeMax ? opts.ATime*length : opts.ATimeMax
    var decayDuration = opts.DTime*length < opts.DTimeMax ? opts.DTime*length : opts.DTimeMax
    // Slope is rise over run
    var slope = (opts.sustainHeight - 1)/decayDuration
    // Starting height + (t-attackDuration)*decayDuration*slope
    // For a linear decay phase return 1-(t-attack duration)*slope
    return 1 + ((t - noteStart)-attackDuration)*decayDuration*slope
  } else if ((t - noteStart) < length - opts.Rtime || (t - noteStart) < length - opts.RTimeMax ) {
    // Sustain phase
    return opts.sustainHeight
  } else {
    // How long the release phase lasts
    var releaseDuration = opts.RTime*length < opts.RTimeMax ? opts.RTime*length : opts.RTimeMax
    // The time shifted so 0 is at the start of the release phase
    var phase_t = (t - noteStart) - (length - releaseDuration)
    // Find the slope
    var slope = (0 - opts.sustainHeight)/releaseDuration
    // Release phase
    return opts.sustainHeight + phase_t*slope
  }
}

//This is the function that actually plays the sound
var play = function (sound, duration, opts, callback) {
  //Give duration a default value
  if (!duration) {
    duration = 0.25;
  }
  if (typeof sound === 'function') {
    b = {};
    b = baudio(function (t) {
      //sample = sound(t, duration, opts)*envelope(t, duration);
      sample = sound(t, duration, opts)*ADSRenvelope(t, duration, opts);
      if (t >= duration) {
        this.end();
      }
      return sample;
    });
    b.play();
  }
  if (typeof callback === 'function') {
    noteStart = noteStart + notes[currentNote].d
    currentNote = currentNote + 1
    setTimeout(callback, duration*1000)
  }
}

var nextNote = function () {
  if (notes.length > currentNote) {
    console.log(currentNote)
    if (notes[currentNote].n) {
      notes[currentNote].o = notes[currentNote].o || {}
      notes[currentNote].o.frequency = frequencyFromNoteNumber(notes[currentNote].n)
    }
    console.log(notes[currentNote].o)
    play(notes[currentNote].t, notes[currentNote].d, notes[currentNote].o, nextNote)
  }
}

var noteStart = 0

var currentNote = 0

var playNotes = function (notes) {
  nextNote()
}

/*
  This takes a note number and returns the frequency from an idealised piano
  49 is A4 = 440hz

  We can add things later to do more realistic tuning options
*/
var frequencyFromNoteNumber = function (number) {
  return 440*Math.pow(2, (number-49)/12)
}

var sounds = {
  upward: upward,
  downward: downward,
  additative: additative,
  FMThing: FMThing
}

var notes = [
  {'t':sounds.upward, 'd':0.7, 'n': 49},
  {'t':sounds.downward, 'd':0.4, 'n': 102},
  {'t':sounds.upward, 'd':0.3, 'n': 20},
  {'t':sounds.downward, 'd':0.5, 'n': 5},
  {'t':sounds.upward, 'd': 0.4, 'n': 70},
  {'t':sounds.FMThing, 'd': 0.4, 'n': 10},
  {'t':sounds.additative, 'd': 0.4, 'n': 150}
]

playNotes()

/*
module.exports.configure = configure;
module.exports.start = start;
module.exports.stop = stop;
module.exports.exit = exit;
module.exports.play = play;
module.exports.sounds = sounds;
*/
